package dk.s4.microservices.plandefinitionservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import dk.s4.microservices.microservicecommon.fhir.FhirCUDEventProcessor;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyEventProcessor extends FhirCUDEventProcessor {
    private final Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);
    private final IFhirResourceDao<PlanDefinition> planDefinitionDao;

    public MyEventProcessor(FhirContext fhirContext, IFhirResourceDao<PlanDefinition> planDefinitionDao) {
        super(fhirContext);
        this.planDefinitionDao = planDefinitionDao;
    }

    @Override
    protected IFhirResourceDao daoForResourceType(String resourceType) {
        if (resourceType.equals("PlanDefinition")) {
            return planDefinitionDao;
        }
        logger.error("No DAO for resource type - should not happen");
        return null;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        Identifier identifier = null;
        if (resource instanceof PlanDefinition) {
            PlanDefinition planDefinition = (PlanDefinition) resource;
            identifier = planDefinition.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(System.getenv("OFFICIAL_PLANDEFINITION_IDENTIFIER_SYSTEM")))
                    .findAny()
                    .orElse(null);
        }
        
        return identifier;
    }
}