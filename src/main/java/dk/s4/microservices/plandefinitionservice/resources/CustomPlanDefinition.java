package dk.s4.microservices.plandefinitionservice.resources;

import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.model.api.annotation.Extension;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.model.api.annotation.SearchParamDefinition;
import ca.uhn.fhir.rest.gclient.ReferenceClientParam;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import ca.uhn.fhir.util.ElementUtil;
import org.hl7.fhir.r4.model.Configuration;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.hl7.fhir.r4.model.Reference;

@ResourceDef(name = "PlanDefinition", profile = CustomPlanDefinition.PROFILE)
public class CustomPlanDefinition extends PlanDefinition {
    public static final String PROFILE = "https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47317015/FHIR+PlanDefinition";

    private static final long serialVersionUID = 1L;

    @Child(name = "managingOrganization")
    @Extension(definedLocally = false, isModifier = false, url = "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100433923/PlanDefinition+managingOrganization+extension")
    @Description(shortDefinition="Organization that manages this PlanDefinition")
    private Reference managingOrganization;

    @SearchParamDefinition(
            name = "managing-organization",
            path = "PlanDefinition.managingOrganization",
            description = "Reference to organization that manages this PlanDefinition",
            type = "reference"
    )
    public static final String SP_MANAGING_ORGANIZATION = "managing-organization";
    public static final ReferenceClientParam MANAGING_ORGANIZATION = new ReferenceClientParam("managing-organization");

    @SearchParamDefinition(
            name = "managing-organization-identifier",
            path = "PlanDefinition.managingOrganization.identifier",
            description = "Identifier of the organization that manages this PlanDefinition",
            type = "token"
    )
    public static final String SP_MANAGING_ORGANIZATION_IDENTIFIER = "managing-organization-identifier";
    public static final TokenClientParam MANAGING_ORGANIZATION_IDENTIFIER = new TokenClientParam("managing-organization-identifier");

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && ElementUtil.isEmpty(managingOrganization);
    }

    public Reference getManagingOrganization() {
        if (this.managingOrganization == null) {
            if (Configuration.errorOnAutoCreate()) {
                throw new Error("Attempt to auto-create Observation.subject");
            }

            if (Configuration.doAutoCreate()) {
                this.managingOrganization = new Reference();
            }
        }

        return managingOrganization;
    }

    public void setManagingOrganization(Reference managingOrganization) {
        this.managingOrganization = managingOrganization;
    }
}
