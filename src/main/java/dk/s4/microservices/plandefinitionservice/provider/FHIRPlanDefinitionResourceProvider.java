package dk.s4.microservices.plandefinitionservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.URIParameter;
import java.util.List;
import java.util.Map;

/**
 * Class that represents a resource provider for the FHIR PlanDefinition resource,
 * which supports the operations: read, create, update, and search.
 * 
 */
public class FHIRPlanDefinitionResourceProvider extends BaseJpaResourceProvider<PlanDefinition> {
	private static final Logger logger = LoggerFactory.getLogger(FHIRPlanDefinitionResourceProvider.class);


	public FHIRPlanDefinitionResourceProvider(IFhirResourceDao<PlanDefinition> dao, FhirContext fhirContext) {
		super(dao);
		setContext(fhirContext);
	}
	/**
	 * The getResourceType method comes from IResourceProvider, and must be
	 * overridden to indicate what type of resource this provider supplies.
	 */
	@Override
	public Class<PlanDefinition> getResourceType() {
		return PlanDefinition.class;
	}

	/**
	 * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
	 * always throw a NotImplementedOperationException.
	 */
	@Override
	@Patch
	public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
			RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
		throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
	}

	/**
	 * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
	 * always throw a NotImplementedOperationException.
	 */
	@Override
	@Read
	public PlanDefinition read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
		throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
	}

	@Search(queryName = "getPlanDefinitionByOrganization")
    public IBundleProvider getPlanDefinitionByOrganization(HttpServletRequest theRequest, RequestDetails theRequestDetails,
                                               HttpServletResponse theResponse,
            @RequiredParam(name= "organization") TokenOrListParam organization) {
        logger.debug("getPlanDefinitionByOrganization");
        try {
            this.startRequest(theRequest);

            SearchParameterMap searchParameterMap = new SearchParameterMap();
            searchParameterMap.add("status", new TokenParam().setValue("active"));
			searchParameterMap.add("managing-organization", organization);

			return this.getDao().search(searchParameterMap, theRequestDetails, theResponse);
		}
		finally {
			this.endRequest(theRequest);
		}
	}

	@Search(queryName = "getByIdentifier")
	public IBundleProvider getByIdentifier(HttpServletRequest theRequest, RequestDetails theRequestDetails,
										  HttpServletResponse theResponse,
										  @RequiredParam(name= "identifier") TokenParam identifier,
										  @OptionalParam(name= "organization") TokenOrListParam organization){
		SearchParameterMap searchParameterMap = new SearchParameterMap();
		searchParameterMap.add("identifier", identifier);
		searchParameterMap.add("managing-organization", organization);
		IBundleProvider PlanDefinitionBundleProvider = this.getDao().search(searchParameterMap, theRequestDetails, theResponse);

		if(PlanDefinitionBundleProvider.size() == 0){
			throw new ResourceNotFoundException("A resource with the given identifier was not found.");
		}
		return PlanDefinitionBundleProvider;
	}


	@Search(queryName = "getByUrl")
	public IBundleProvider getByUrl(HttpServletRequest theRequest, RequestDetails theRequestDetails, HttpServletResponse theResponse,
						   @RequiredParam(name = PlanDefinition.SP_URL) UriParam url,
						   @OptionalParam(name= "organization") TokenOrListParam organization) {
		try {
			SearchParameterMap searchParameterMap = new SearchParameterMap();
			searchParameterMap.add("url", url);
			searchParameterMap.add("managing-organization", organization);

			return this.getDao().search(searchParameterMap, theRequestDetails, theResponse);
		} finally {
			this.endRequest(theRequest);
		}
	}


	/**
	 * Generic search API for retrieval of Bundles of PlanDefinitions stored by this service
	 *
	 * @param theServletRequest Incoming http request
	 * @param theServletResponse	Outgoing http response
	 * @param theRequestDetails The details of the http request
	 * @param theFtContent Search the contents of the resource's data using a fulltext search
	 * @param theHas Return resources linked to by the given target
	 * @param the_id The ID of the resource
	 * @param theIdentifier A PlanDefinition's Identifier
	 * @param theVersion The business version of the plan definition
	 * @param theTitle The human-friendly name of the plan definition
	 * @param theStatus The status of the PlanDefinition
	 * @param theLastUpdated Only return resources which were last updated as specified by the given range
	 * @param thePublisher Name of the publisher of the plan definition
	 * @param theJurisdiction Intended jurisdiction for the plan definition
	 * @param theDescription The description of the plan definition
	 * @param theDefinition Activity or plan definitions used by plan definition
	 * @param theSort Sort order
	 * @param theCount
	 * @return Bundle of PractitionerRole resources
	 */

	@Search(allowUnknownParams=true)
	public IBundleProvider search(HttpServletRequest theServletRequest,
								  HttpServletResponse theServletResponse,
								  RequestDetails theRequestDetails,
								  @Description(shortDefinition = "Search the contents of the resource's data using a fulltext search")
								  @OptionalParam(name = "_content") StringAndListParam theFtContent,
								  @Description(shortDefinition = "Return resources linked to by the given target")
								  @OptionalParam(name = "_has") HasAndListParam theHas,
								  @Description(shortDefinition = "The ID of the resource")
								  @OptionalParam(name = "_id") TokenAndListParam the_id,
								  @Description(shortDefinition = "A PlanDefinition's Identifier")
								  @OptionalParam(name = "identifier") TokenAndListParam theIdentifier,
								  @Description(shortDefinition="The Canonical identifier for PlanDefinition")
								  @OptionalParam(name = PlanDefinition.SP_URL) UriParam url,
								  @Description(shortDefinition="The business version of the plan definition")
								  @OptionalParam(name="version") TokenAndListParam theVersion,
								  @Description(shortDefinition="The human-friendly name of the plan definition")
								  @OptionalParam(name="title") StringAndListParam theTitle,
								  @Description(shortDefinition = "The current status of the PlanDefinition")
								  @OptionalParam(name = "status") TokenAndListParam theStatus,
								  @Description(shortDefinition="Name of the publisher of the plan definition")
								  @OptionalParam(name="publisher") StringAndListParam thePublisher,
								  @Description(shortDefinition="Intended jurisdiction for the plan definition")
								  @OptionalParam(name="jurisdiction") TokenAndListParam theJurisdiction,
								  @Description(shortDefinition="The description of the plan definition")
								  @OptionalParam(name="description") StringAndListParam theDescription,
								  @Description(shortDefinition="The managing organization of the plan definition")
								  @OptionalParam(name="managing-organization") TokenOrListParam theManagingOrganization,
								  @Description(shortDefinition="Activity or plan definitions used by plan definition")
								  @OptionalParam(name="definition", targetTypes={  } ) ReferenceAndListParam theDefinition,
								  @Description(shortDefinition = "Only return resources which were last updated as specified by the given range")
								  @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,

			                      @RawParam Map<String, List<String>> theAdditionalRawParams,

			                      @Sort SortSpec theSort,
								  @Count Integer theCount) {
		this.startRequest(theServletRequest);

		IBundleProvider iBundleProvider;
		try {
			SearchParameterMap paramMap = new SearchParameterMap();
			paramMap.add("_content", theFtContent);
			paramMap.add("_has", theHas);
			paramMap.add("_id", the_id);
			paramMap.add("identifier", theIdentifier);
			paramMap.add("version", theVersion);
			paramMap.add("title", theTitle);
			paramMap.add("status", theStatus);
			paramMap.add("publisher", thePublisher);
			paramMap.add("jurisdiction", theJurisdiction);
			paramMap.add("description", theDescription);
			paramMap.add("managing-organization",theManagingOrganization);
			paramMap.add("definition", theDefinition);
			paramMap.add("url", url);
			paramMap.setLastUpdated(theLastUpdated);
			paramMap.setSort(theSort);
			paramMap.setCount(theCount);

			getDao().translateRawParameters(theAdditionalRawParams, paramMap);

			IBundleProvider retVal = this.getDao().search(paramMap, theRequestDetails, theServletResponse);
			iBundleProvider = retVal;
		} finally {
			this.endRequest(theServletRequest);
		}

		return iBundleProvider;
	}

}
