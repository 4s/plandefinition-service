package dk.s4.microservices.plandefinitionservice.servlet;

import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.api.Constants;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.servlet.ServletRequestDetails;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.plandefinitionservice.resources.CustomPlanDefinition;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomPlanDefinitionTest {
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(CustomPlanDefinitionTest.class);
    private static final String ENVIRONMENT_FILE = "service.env";
    private static final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private List<IIdType> createdObjects;
    private Server ourServer;
    private RequestDetails noCachingRequestDetails;

    @Test
    public void testSaveAndRestore() {
        {
            // Ensure DB is empty
            IBundleProvider found = PlanDefinitionService.getPlanDefinitionDao().search(new SearchParameterMap(), noCachingRequestDetails);
            assertThat(found.size().intValue()).isEqualTo(0);
        }

        CustomPlanDefinition planDefinition = new CustomPlanDefinition();
        Reference orgRef = new Reference().setIdentifier(new Identifier().setValue("1234").setSystem("my.test.system"));
        planDefinition.setName("my1234org");
        planDefinition.setManagingOrganization(orgRef);

        IIdType id = PlanDefinitionService.getPlanDefinitionDao().create(planDefinition).getId().toUnqualifiedVersionless();
        createdObjects.add(id);

        CustomPlanDefinition read = (CustomPlanDefinition) PlanDefinitionService.getPlanDefinitionDao().read(id);
        assertThat("1234").isEqualTo(read.getManagingOrganization().getIdentifier().getValue());

        IBundleProvider found = PlanDefinitionService.getPlanDefinitionDao().search(new SearchParameterMap(), noCachingRequestDetails);
        assertThat(found.size().intValue()).isEqualTo(1);
        CustomPlanDefinition search = (CustomPlanDefinition) found.getResources(0, 1).get(0);
        System.out.println(search);
        assertThat("1234").isEqualTo(search.getManagingOrganization().getIdentifier().getValue());
    }

    @After
    public void after() throws Exception {
        LOGGER.info("Shutting down service");
        for (IIdType id : createdObjects) {
            LOGGER.info("Deleting: " + id);
            PlanDefinitionService.getPlanDefinitionDao().delete(id);
        }
        PlanDefinitionService.getPlanDefinitionDao().getContext().setDefaultTypeForProfile(CustomPlanDefinition.PROFILE, null);
        ourServer.stop();
    }

    @Before
    public void setup() throws Exception {
        noCachingRequestDetails = createRequestDetailsThatDisableCaching();
        createdObjects = new ArrayList<>();

        //Use Derby local file based database
        String value = System.getenv("DATABASE_TYPE");
        if (value == null) {
            environmentVariables.set("DATABASE_TYPE", "Derby");
        }

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment(ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml", "target/service", environmentVariables, ourPort, ourServer);

        // Instantiate mock Kafka classes
        TestUtils.initMockExternalProducer();
        TestUtils.initMockKafkaConsumeAndProcess(PlanDefinitionService.getTopics(), PlanDefinitionService.getEventProcessor());
    }

    private RequestDetails createRequestDetailsThatDisableCaching() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getHeaders(Constants.HEADER_CACHE_CONTROL)).thenReturn(Collections.enumeration(Collections.singletonList(Constants.CACHE_CONTROL_NO_CACHE)));
        ServletRequestDetails requestDetails = new ServletRequestDetails();
        requestDetails.setServletRequest(request);
        return requestDetails;
    }

}