package dk.s4.microservices.plandefinitionservice.servlet;

import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.Constants;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.servlet.ServletRequestDetails;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Message.Prefer;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

public class EventProcessingTest {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(EventProcessingTest.class);

    private static long FUTURE_TIMEOUT = 15000;

    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;
    private static Server ourServer;
    private static RequestDetails noCachingRequestDetails;
    private List<IIdType> createdObjects;

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        noCachingRequestDetails = createRequestDetailsThatDisableCaching();
        EnvironmentVariables environmentVariables = new EnvironmentVariables();
        //Use Derby local file based database
        if (System.getenv("DATABASE_TYPE") == null) {
            environmentVariables.set("DATABASE_TYPE", "Derby");
        }

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml", "target/service", environmentVariables, ourPort, ourServer);

        // Instantiate mock Kafka classes
        mockExternalKafkaProducer = TestUtils.initMockExternalProducer();
        mockKafkaEventProducer = TestUtils.initMockKafkaConsumeAndProcess(PlanDefinitionService.getTopics(), PlanDefinitionService.getEventProcessor());
    }

    private static RequestDetails createRequestDetailsThatDisableCaching() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getHeaders(Constants.HEADER_CACHE_CONTROL)).thenReturn(Collections.enumeration(Collections.singletonList(Constants.CACHE_CONTROL_NO_CACHE)));
        ServletRequestDetails requestDetails = new ServletRequestDetails();
        requestDetails.setServletRequest(request);
        return requestDetails;
    }

    @Test
    public void testCreatePlanDefinition() throws Exception {
        {
            // Ensure DB is empty
            IBundleProvider found = PlanDefinitionService.getPlanDefinitionDao().search(new SearchParameterMap(), noCachingRequestDetails);
            assertThat(found.size().intValue()).isEqualTo(0);
        }
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/PlanDefinition.json");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(resourceString);
        message.setSender("EventProcessingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("PlanDefinition");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        Topic inputTopic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("PlanDefinition");

        Topic outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("PlanDefinition");

        ProducerRecord<String, String> record = new ProducerRecord<>(inputTopic.toString(), message.toString());

        LOGGER.trace("Sending message to KafkaProducer: " + message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(FUTURE_TIMEOUT * 2, TimeUnit.MILLISECONDS);
        LOGGER.debug("Message sent to KafkaProducer: offset = " + metadata.offset());

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);

        assertThat(message.getBodyCategory()).isEqualTo(outputMessage.getBodyCategory());
        assertThat("PlanDefinition").isEqualTo(outputMessage.getBodyType());
        assertThat(message.getCorrelationId()).isEqualTo(outputMessage.getCorrelationId());
        assertThat(message.getTransactionId()).isEqualTo(outputMessage.getTransactionId());
        assertThat(message.getContentVersion()).isEqualTo(outputMessage.getContentVersion());
        assertNotNull(metadata);

        IBundleProvider found = PlanDefinitionService.getPlanDefinitionDao().search(new SearchParameterMap(), noCachingRequestDetails);
        assertThat(found.size().intValue()).isEqualTo(1);
        createdObjects.add(found.getResources(0, 1).get(0).getIdElement().toUnqualifiedVersionless());
    }

    @Before
    public void setup() {
        createdObjects = new ArrayList<>();
    }

    @After
    public void tearDown() {
        for (IIdType id : createdObjects) {
            PlanDefinitionService.getPlanDefinitionDao().delete(id);
        }
    }

    @Test
    public void testUpdatePlanDefinition() throws Exception {
        {
            // Ensure DB is empty
            IBundleProvider found = PlanDefinitionService.getPlanDefinitionDao().search(new SearchParameterMap(), noCachingRequestDetails);
            assertThat(found.size().intValue()).isEqualTo(0);
        }
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/PlanDefinition.json");

        IParser parser = PlanDefinitionService.getServerFhirContext().newJsonParser();

        PlanDefinition storedPlanDefinition = parser.parseResource(PlanDefinition.class, resourceString);
        IIdType id = PlanDefinitionService.getPlanDefinitionDao().create(storedPlanDefinition).getId();
        PlanDefinition planDefinition = PlanDefinitionService.getPlanDefinitionDao().read(id);
        createdObjects.add(id.toUnqualifiedVersionless());

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(parser.encodeResourceToString(planDefinition));
        message.setSender("EventProcessingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("PlanDefinition");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        Topic inputTopic = new Topic()
                .setOperation(Operation.Update)
                .setDataCategory(Category.FHIR)
                .setDataType("PlanDefinition");

        Topic outputTopic = new Topic()
                .setOperation(Operation.DataUpdated)
                .setDataCategory(Category.FHIR)
                .setDataType("PlanDefinition");

        ProducerRecord<String, String> record = new ProducerRecord<>(inputTopic.toString(), message.toString());

        LOGGER.trace("Sending message to KafkaProducer: " + message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(FUTURE_TIMEOUT * 2, TimeUnit.MILLISECONDS);
        LOGGER.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);

        assertThat(message.getBodyCategory()).isEqualTo(outputMessage.getBodyCategory());
        assertThat("PlanDefinition").isEqualTo(outputMessage.getBodyType());
        assertThat(message.getCorrelationId()).isEqualTo(outputMessage.getCorrelationId());
        assertThat(message.getTransactionId()).isEqualTo(outputMessage.getTransactionId());
        assertThat(message.getContentVersion()).isEqualTo(outputMessage.getContentVersion());
        assertNotNull(metadata);

        IBundleProvider found = PlanDefinitionService.getPlanDefinitionDao().search(new SearchParameterMap(), noCachingRequestDetails);
        assertThat(found.size().intValue()).isEqualTo(1);
    }
}
