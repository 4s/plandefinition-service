package dk.s4.microservices.plandefinitionservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class RestfulFHIRProvidersTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);
    private static IGenericClient ourClient;
    private static Server ourServer;
    private static String ourServerBase;
    private static List<IIdType> createdObjects;
    private static String PD_URL = "";

    @AfterClass
    public static void afterClass() throws Exception {
        for (IIdType id : createdObjects) {
            LOGGER.info("Deleting: " + id);
            PlanDefinitionService.getPlanDefinitionDao().delete(id);
        }
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        createdObjects = new ArrayList<>();
        EnvironmentVariables environmentVariables = new EnvironmentVariables();
        if (System.getenv("DATABASE_TYPE") == null) {
            environmentVariables.set("DATABASE_TYPE", "Derby");
        }
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml", "target/service", environmentVariables, ourPort, ourServer);

        //Init FHIR Client
        FhirContext fhirContext = PlanDefinitionService.getServerFhirContext();
        ourClient = fhirContext.newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));

        IParser jsonParser = PlanDefinitionService.getServerFhirContext().newJsonParser();

        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/PlanDefinition.json");
        PlanDefinition planDefinition = jsonParser.parseResource(PlanDefinition.class, resourceString);

        PD_URL = planDefinition.getUrl();

        IIdType id = PlanDefinitionService.getPlanDefinitionDao().create(planDefinition).getId();
        createdObjects.add(id.toUnqualifiedVersionless());

        PlanDefinitionService.forceReindexing();
    }

    @Test
    public void testPlanDefinitionReadNotSupported() {
        Throwable throwable = catchThrowable(() ->
                ourClient.read().resource(PlanDefinition.class).withId("PlanDefinition/" + "1").execute());
        assertThat(throwable).isInstanceOf(NotImplementedOperationException.class);
    }

    @Test
    public void testPlanDefinitionPatchNotSupported() {
        Throwable throwable = catchThrowable(() ->
                ourClient.patch().
                        withBody("[ { \"op\":\"replace\", \"path\":\"/active\", \"value\":false } ]").
                        withId("PlanDefinition/1").prefer(PreferReturnEnum.OPERATION_OUTCOME).execute());
        assertThat(throwable).isInstanceOf(NotImplementedOperationException.class);
    }

    @Test
    public void testPlanDefinitionCreateNotSupported() {
        PlanDefinition planDefinition = new PlanDefinition();
        planDefinition.setName("testPlanDefinition");
        Throwable throwable = catchThrowable(() ->
                ourClient.create().resource(planDefinition).execute());
        assertThat(throwable).isInstanceOf(InvalidRequestException.class);
    }

    @Test
    public void testPlanDefinitionSearch() {
        String searchUrl = ourServerBase + "/PlanDefinition?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetPlanDefinitionByOrganization() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getPlanDefinitionByOrganization&organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testDaoSearch() {
        String searchUrl = ourServerBase + "/PlanDefinition?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testManagingOrganisationSearch() {
        String searchUrl = ourServerBase + "/PlanDefinition?managing-organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void testManagingOrganisationInvalidValueSearch() {
        String searchUrl = ourServerBase + "/PlanDefinition?managing-organization=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }


    @Test
    public void testManagingOrganisationInvalidSearch() {
        String searchUrl = ourServerBase + "/PlanDefinition?managing-organization=invalid|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetByIdentifier() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|d1347528-4b1e-400a-88c9-a5b0f842a36b"
                + "&organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetByIdentifierWrongOrganization() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|5f60c9f9-5a20-4045-863f-0b0b310c24de"
                + "&organization=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Throwable throwable = catchThrowable(query::execute);
        assertThat(throwable).isInstanceOf(ResourceNotFoundException.class);
    }


    @Test
    public void testGetByIdentifierWrongIdentifier() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByIdentifier&identifier=urn:oid:1.2.208.176.1.1|1234567890"
                + "&organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Throwable throwable = catchThrowable(query::execute);
        assertThat(throwable).isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    public void testGetByUrl() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByUrl&url=" + PD_URL;
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetByUrlWithOrganization() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByUrl&url=" + PD_URL
                + "&organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetByUrlWithInvalidOrganization() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByUrl&url=" + PD_URL
                + "&organization=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetByUrlInvalidUrl() {
        String searchUrl = ourServerBase + "/PlanDefinition?_query=getByUrl&url=" + "PD_URL";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }
}
